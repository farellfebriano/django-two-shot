from django.contrib import admin
from receipts.models import Receipt, Account, ExpenseCategory

# Register your models here.


@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    class Meta:
        fields = (
            "name",
            "owner",
        )


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    class Meta:
        fields = (
            "name",
            "number",
            "owner",
        )


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    class Meta:
        fields = (
            "vendor",
            "total",
            "tax",
            "date",
            "purchaser",
            "category",
            "account",
        )
