from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from .form import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth import authenticate


@login_required
def receipt(request):
    form = Receipt.objects.filter(purchaser=request.user)  # was request.user
    context = {"receipt": form}
    return render(request, "org/receipt.html", context)


@login_required
def receipt_form(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        print(
            form,
            "----------------------------FORM--------------------------------",
        )
        if form.is_valid:
            receipt = form.save(False)
            print(
                receipt,
                "----------------------------RECEIPT--------------------------",
            )
            receipt.purchaser = request.user
            form.save()  # in scrumptios it wants the receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"form": form}
    return render(request, "org/create_receipt.html", context)


@login_required
# list of category with number of item inside it
def category_list(request):
    # if authenticate(request.owner):
    form = ExpenseCategory.objects.filter(owner=request.user)
    context = {"category_list": form}
    return render(request, "org/category_list.html", context)
    # else:
    #     return redirect("home")


@login_required
# createing a account list
def account_list(request):
    # if authenticate(request.user):
    form = Account.objects.filter(owner=request.user)
    context = {"account_list": form}
    return render(request, "org/account_list.html", context)
    # else:
    #     return redirect("home")


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            user = form.save(False)
            print(request.user, "---------------------------------")
            user.owner = request.user
            user.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {"form": form}
    return render(request, "org/create_category.html", context)


def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid:
            user = form.save(False)
            user.owner = request.user
            user.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "org/create_account.html", context)
