from django.urls import path
from .views import login_form, log_out, sign_up

urlpatterns = [
    path("login/", login_form, name="login"),
    path("logout/", log_out, name="logout"),
    path("signup/", sign_up, name="signup"),
]
