from django.shortcuts import render, redirect

# Create your views here.

# creating a login form
# from accounts.login_form import LoginForm
from django.contrib.auth import authenticate, login
from accounts.forms import LoginForm
from .models import SignUp
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.contrib.auth import logout


def login_form(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        # print(form, "this is form")
        if form.is_valid():
            username = form.cleaned_data["username"]
            # print(username)
            password = form.cleaned_data["password"]
            # print(password)
            user = authenticate(request, username=username, password=password)
            # print(user)
            if user is not None:
                login(request, user)
                return redirect("home")

    else:
        form = LoginForm()

    context = {
        "login_form": form,
    }
    return render(request, "accounts/login.html", context)


# making log out page


def log_out(request):
    logout(request)
    return redirect("login")


# makinf a signup form


def sign_up(request):
    if request.method == "POST":
        form = SignUp(request.POST)
        print(form)
        # print(form, "----------------------------------form")
        if form.is_valid:
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(
                    username=username,
                    password=password,
                )
                login(request, user)
                return redirect("home")
            else:
                return HttpResponse("the passwords do not match")
    else:
        form = SignUp()
    # print(form, "-----------------------------------------form-")
    context = {"form": form}
    return render(request, "registration/signup.html", context)
